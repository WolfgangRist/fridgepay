//
//  FridgePayTests.swift
//  FridgePayTests
//
//  Created by Wolfgang Rist on 09.03.19.
//  Copyright © 2019 marathonpeople. All rights reserved.
//

import XCTest
@testable import FridgePay

class FridgePayTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testCanDecodeProductInfo() {
        let serializer = JSONSerializer()
        serializer.serialize(input: "products") { (productInfo) in
            print("productInfo: \(String(describing: productInfo)))")

            XCTAssertNotNil(productInfo)
            XCTAssertEqual(productInfo?.products.count, 35)
        }
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
}
