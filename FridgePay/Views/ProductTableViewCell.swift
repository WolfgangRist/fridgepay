//
//  ProductTableViewCell.swift
//  FridgePay
//
//  Created by Wolfgang Rist on 09.03.19.
//  Copyright © 2019 marathonpeople. All rights reserved.
//


import UIKit
import RxSwift
import RxCocoa
import Kingfisher

class ProductTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var quantityLabel: UILabel!
    
    private var viewModel: ProductViewModel!
    private var product: Product!
    
    func configure(viewModel: ProductViewModel, product: Product) {
        self.viewModel = viewModel
        self.product = product
        
        nameLabel?.text = product.name
        priceLabel?.text = viewModel.price(product: product)
        quantityLabel?.text = "\(product.quantity)"
        if let imageUrl = viewModel.imageUrl(product: product), let url = URL(string: imageUrl) {
            productImageView.kf.setImage(with: url)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func buyAction(_ sender: UIButton) {
        viewModel.purchase(product: product)
    }
    
}

