//
//  ProductList.swift
//  FridgePay
//
//  Created by Wolfgang Rist on 09.03.19.
//  Copyright © 2019 marathonpeople. All rights reserved.
//

import Foundation

struct ProductInfo {
    var version: Int
    var updatedAt: String
    var imagePath: String
    var placeHolderImagePath: String
    var currency: String
    var contentVolumeUnit: String
    var products: [Product]
    
    init(version: Int, updatedAt: String, imagePath: String, placeHolderImagePath: String, currency: String, contentVolumeUnit: String, products: [Product]) {
        self.version = version
        self.updatedAt = updatedAt
        self.imagePath = imagePath
        self.placeHolderImagePath = placeHolderImagePath
        self.currency = currency
        self.contentVolumeUnit = contentVolumeUnit
        self.products = products
    }
}

extension ProductInfo: Decodable {
    enum CodingKeys: String, CodingKey {
        case version = "version"
        case updatedAt = "updatedAt"
        case imagePath = "imagePath"
        case placeHolderImagePath = "placeHolderImagePath"
        case currency = "currency"
        case contentVolumeUnit = "contentVolumeUnit"
        case products = "products"
    }
            
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let version: Int = try container.decode(Int.self, forKey: .version)
        let updatedAt: String = try container.decode(String.self, forKey: .updatedAt)
        let imagePath: String = try container.decode(String.self, forKey: .imagePath)
        let placeHolderImagePath: String = try container.decode(String.self, forKey: .placeHolderImagePath)
        let currency: String = try container.decode(String.self, forKey: .currency)
        let contentVolumeUnit: String = try container.decode(String.self, forKey: .contentVolumeUnit)
        let products = try container.decodeIfPresent([Product].self, forKey: .products) ?? []
        self.init(version: version, updatedAt: updatedAt, imagePath: imagePath, placeHolderImagePath: placeHolderImagePath, currency: currency, contentVolumeUnit: contentVolumeUnit, products: products)
    }
}

