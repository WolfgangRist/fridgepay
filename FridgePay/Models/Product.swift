//
//  Product.swift
//  FridgePay
//
//  Created by Wolfgang Rist on 09.03.19.
//  Copyright © 2019 marathonpeople. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

@objcMembers final class Product: Object {
    dynamic var id: Int = 0
    dynamic var type: String = ""
    dynamic var name: String = ""
    dynamic var contentVolume: Int = 0
    dynamic var price: Double = 0.0
    dynamic var imageName: String = ""
    
    dynamic var quantity: Int = 0

    convenience init(id: Int, type: String, name: String, contentVolume: Int, price: Double, imageName: String) {
        self.init()
        self.id = id
        self.type = type
        self.name = name
        self.contentVolume = contentVolume
        self.price = price
        self.imageName = imageName
    }
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

extension Product: Decodable {
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case type = "type"
        case name = "name"
        case contentVolume = "contentVolume"
        case price = "price"
        case imageName = "imageName"
    }
    
    convenience init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let id: Int = try container.decode(Int.self, forKey: .id)
        let type: String = try container.decode(String.self, forKey: .type)
        let name: String = try container.decode(String.self, forKey: .name)
        let contentVolume: Int = try container.decode(Int.self, forKey: .contentVolume)
        let price: Double = try container.decode(Double.self, forKey: .price)
        let imageName: String = try container.decode(String.self, forKey: .imageName)
        
        self.init(id: id, type: type, name: name, contentVolume: contentVolume, price: price, imageName: imageName)
    }
}

extension Product: Comparable {
    static func < (lhs: Product, rhs: Product) -> Bool {
        return lhs.name < rhs.name
    }
    static func == (lhs: Product, rhs: Product) -> Bool {
        return lhs.name == rhs.name && lhs.id == rhs.id
    }
}
