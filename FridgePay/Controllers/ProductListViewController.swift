//
//  ProductListViewController.swift
//  FridgePay
//
//  Created by Wolfgang Rist on 09.03.19.
//  Copyright © 2019 marathonpeople. All rights reserved.
//

import UIKit
import RealmSwift
import RxSwift
import RxCocoa
import RxRealm

class ProductListViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var payButton: UIButton!
    
    private let disposeBag = DisposeBag()
    private let viewModel = ProductViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        bindViewModel()
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private func bindViewModel() {
        bindTableView()
        bindPurchaseView()

        viewModel.fetchProducts()
    }
    
    private func bindTableView() {
        viewModel.changeSet
            .subscribe(onNext: {[weak self] changes in
                let changesTuple: ChangeSetType = changes
                if !changesTuple.deleted.isEmpty || !changesTuple.inserted.isEmpty || !changesTuple.updated.isEmpty {
                    self?.tableView.applyChangeset(changes)
                } else {
                    self?.tableView.reloadData()
                }
            })
            .disposed(by: disposeBag)
    }
    
    private func bindPurchaseView() {
        viewModel.totalPrice
            .bind(to: totalPriceLabel.rx.text)
            .disposed(by: disposeBag)

        viewModel.isPayButtonEnabled
            .bind(to: payButton.rx.isEnabled)
            .disposed(by: disposeBag)
    }
    
    @IBAction func payAction(_ sender: UIButton) {
        viewModel.pay()
    }
}

extension ProductListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell") as? ProductTableViewCell else { return UITableViewCell() }
        cell.configure(viewModel: viewModel, product: viewModel.products[indexPath.row])
        return cell
    }
}

extension ProductListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        defer {
            tableView.deselectRow(at: indexPath, animated: true)
        }
        let product = viewModel.products[indexPath.row]
        viewModel.purchase(product: product)
    }
}

