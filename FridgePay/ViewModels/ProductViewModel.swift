//
//  ProductViewModel.swift
//  FridgePay
//
//  Created by Wolfgang Rist on 09.03.19.
//  Copyright © 2019 marathonpeople. All rights reserved.
//

import Foundation
import RxSwift
import RealmSwift
import RxRealm

class ProductViewModel {
    
    // MARK: Public properties
    var products: Results<Product>
    
    var isLoading: Observable<Bool> {
        return _isLoading.asObservable().distinctUntilChanged()
    }
    var currency: Observable<String> {
        return _currency.asObservable().distinctUntilChanged()
    }
    var placeHolderImagePath: Observable<String> {
        return _placeHolderImagePath.asObservable().distinctUntilChanged()
    }
    var imagePath: Observable<String> {
        return _imagePath.asObservable().distinctUntilChanged()
    }
    var totalPrice: Observable<String> {
        return _totalPrice.asObservable()
    }
    var changeSet: Observable<ChangeSetType> {
        return _changeSet.asObservable()
    }
    var isPayButtonEnabled: Observable<Bool> {
        return _isPayButtonEnabled.asObservable().distinctUntilChanged()
    }
    
    // MARK: Private properties
    private let _isLoading = Variable(false)
    private let _currency = Variable("")
    private let _placeHolderImagePath = Variable("")
    private let _imagePath = Variable("")
    private let _totalPrice = Variable("")
    private let _changeSet = Variable((deleted: [Int](), inserted: [Int](), updated: [Int]()))
    private let _isPayButtonEnabled = Variable(false)

    private let realmManager = RealmManager()
    private let disposeBag = DisposeBag()
    
    private lazy var currencyNumberFormatter: NumberFormatter = {
        let numberFormatter = NumberFormatter()
//        currencyFormatter.locale = Locale.current
        numberFormatter.locale = Locale(identifier: "de_DE") // force use of German EUR formatting
        numberFormatter.numberStyle = .currency
        return numberFormatter
    }()
    
    init() {
        products = realmManager.noProducts()
    }

    // MARK: Public methods

    public func fetchProducts() {
        _isLoading.value = true
        RxRequest.default.get(returnType: ProductInfo.self, path: "https://projekte.raysono.com/mobile_team/fridge_pay/v1/products.json")
            .map { [weak self] (productInfo) -> [Product] in
                self?._currency.value = productInfo.currency
                self?._placeHolderImagePath.value = productInfo.placeHolderImagePath
                self?._imagePath.value = productInfo.imagePath
                return productInfo.products
            }
            .do(onNext: { (products) in
                print("products: \(products)")
            })
            .subscribe(onNext: { [weak self] products in
                self?._isLoading.value = false
                self?.realmManager.insertProducts(products)
            })
            .disposed(by: disposeBag)
        
        realmManager.products(filter: nil)
            .subscribe(onNext: { products in
                self.products = products
            })
            .disposed(by: disposeBag)
        
        Observable.changeset(from: products)
            .subscribe(onNext: {[weak self] results, changes in
                guard let self = self else { return }
                if let changes = changes {
                    self._changeSet.value = (changes.deleted, changes.inserted, changes.updated)
                } else {
                    self._changeSet.value = ([], [], [])
                }
                let price = self.calcTotalPrice()
                self._totalPrice.value = self.currencyNumberFormatter.string(from: price as NSNumber) ?? ""
                self._isPayButtonEnabled.value = self.calcTotalPrice() > 0
            })
            .disposed(by: disposeBag)
    }
    
    func purchase(product: Product) {
        realmManager.purchase(product: product)
    }
    
    func pay() {
        realmManager.pay()
    }
    
    func price(product: Product) -> String? {
        return currencyNumberFormatter.string(from: product.price as NSNumber)
    }
    
    func imageUrl(product: Product) -> String? {
        if product.imageName.isEmpty {
            return _placeHolderImagePath.value
        } else {
            return _imagePath.value + product.imageName
        }
    }
    
    // MARK: Private methods

    private func calcTotalPrice() -> Double {
        let price = products.reduce(0.0) { (result, product) -> Double in
            result + Double(product.quantity) * product.price
        }
        return price
    }
    
}
