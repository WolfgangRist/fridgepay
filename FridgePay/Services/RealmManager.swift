//
//  RealmManager.swift
//  FridgePay
//
//  Created by Wolfgang Rist on 09.03.19.
//  Copyright © 2019 marathonpeople. All rights reserved.
//

import Foundation
import RealmSwift
import RxSwift
import RxRealm

struct RealmManager {
    
    let disposeBag = DisposeBag()
    
    func isPurchased(id: Int) -> Observable<Product>{
        guard let realm = try? Realm() else {
            return Observable.empty()
        }
        guard let product = realm.object(ofType: Product.self, forPrimaryKey: id) else {
            return Observable.empty()
        }
        if product.quantity > 0 {
            return Observable.from(object: product)
        } else {
            return Observable.empty()
        }
    }
    
    func purchase(product: Product) {
        Observable.just(product)
            .subscribe(onNext: { product in
                let realm = try! Realm()
                try! realm.write {
                    product.quantity += 1
                    realm.add(product, update: true)
                }
            })
            .disposed(by: disposeBag)
    }
    
    func pay() {
        let realm = try! Realm()
        for product in realm.objects(Product.self) {
            try! realm.write {
                product.quantity = 0
            }
        }
    }
    
    func noProducts() -> Results<Product> {
        let realm = try! Realm()
        return realm.objects(Product.self).filter(NSPredicate(value: false))
    }
    
    func products(filter predicateFormat: String? = nil) -> Observable<Results<Product>> {
        guard let realm = try? Realm() else {
            return Observable.empty()
        }
        var results: Results<Product> = realm.objects(Product.self)
        if let predicate = predicateFormat {
            results = results.filter(predicate)
        }
        return Observable.collection(from: results.sorted(byKeyPath: Product.CodingKeys.name.rawValue))
    }
    
    func insertProducts(_ products: [Product]) {
        guard let realm = try? Realm() else { return }
        products.forEach { product in
            if realm.object(ofType: Product.self, forPrimaryKey: product.id) == nil {
                // product does not exist in realm ... insert
                try! realm.write {
                    realm.add(product)
                }
            }
        }
    }
}
