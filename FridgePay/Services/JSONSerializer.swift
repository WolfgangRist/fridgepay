//
//  JSONSerializer.swift
//  FridgePayTests
//
//  Created by Wolfgang Rist on 09.03.19.
//  Copyright © 2019 marathonpeople. All rights reserved.
//

import Foundation

class JSONSerializer {
    func serialize(input sourceName: String, completion: (_ productInfo: ProductInfo?) -> Void) {
        let path = Bundle.main.path(forResource: sourceName, ofType: "json")
        let url = URL(fileURLWithPath: path!)
        let jsonDecoder = JSONDecoder()
        do {
            let data = try Data(contentsOf: url)
            do {
                let productInfo = try jsonDecoder.decode(ProductInfo.self, from: data)
                completion(productInfo)
            } catch {
                print("failed to convert data")
                completion(nil)
            }
        } catch let error {
            completion(nil)
            print(error)
        }
    }
}
