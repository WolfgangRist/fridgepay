//
//  UITableView.swift
//  FridgePay
//
//  Created by Wolfgang Rist on 10.03.19.
//  Copyright © 2019 marathonpeople. All rights reserved.
//

import UIKit
import RxRealm

typealias ChangeSetType = (deleted: [Int], inserted: [Int], updated: [Int])

extension UITableView {
    func applyChangeset(_ changes: ChangeSetType) {
        beginUpdates()
        deleteRows(at: changes.deleted.map { IndexPath(row: $0, section: 0) }, with: .automatic)
        insertRows(at: changes.inserted.map { IndexPath(row: $0, section: 0) }, with: .automatic)
        reloadRows(at: changes.updated.map { IndexPath(row: $0, section: 0) }, with: .automatic)
        endUpdates()
    }
}
